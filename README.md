# Agosti-Intro: Logo mit Liquid-Effekt on MouseOver
**[Link zu Online-Demo](https://michi-r.ch/projects/agosti-intro/)**

## HTML-Markup für Intro-Section 
Befindet sich im `index.html`

	<div class="intro" id="intro-container">
        <div class="intro__content">
            <div class="intro__canvas" id="intro__canvas"></div>
            <div class="intro__flowmap" id="intro__flowmap">
                <img src="img/intro-layer-logo.svg" crossorigin="" data-sampler="planeTexture" alt="Agosti - Die Malermeister">
            </div>
        </div>
        <div class="intro__cover"></div>
    </div>

## CSS-Styles
Befinden sich im File `style.css`

## Javascript
[curtains.js](https://www.curtainsjs.com/)-Library und `curtains.liquidlogo.js` müssen referenziert werden.

    <script src="js/curtains.umd.min.js"></script>
    <script src="js/curtains.liquidlogo.js"></script>